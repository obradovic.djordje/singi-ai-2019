def bfs(start_state, maze, goal, ignore_states=[]):
    unvisited = [start_state]
    visited = []

    while len(unvisited) > 0:
        to_visit = unvisited.pop(0)

        if to_visit.tile.get(goal) and to_visit not in ignore_states:
            return (to_visit, visited)

        if to_visit not in visited:
            visited.append(to_visit)
            next_states = to_visit.get_next_states(maze)
            for ns in next_states:
                if ns not in visited:
                    unvisited.append(ns)

    return (None, visited)
