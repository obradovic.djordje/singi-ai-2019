# singi-ai-2019

Raspored tema po nedeljama nastave: 

## 1) 27.2.2019.sreda
  - uvod u Vestacku inteligenciju
  - postavljanje okruzenja: Anaconda, python 3, jupyter notebook
  - student ce moci da instalira i podesi okruzenje za rad na projektima koji podrazumevaju koriscenje modula za AI

## 2) 6.3.2019. sreda
  - Slepe pretrage, BFS, DFS, Iterativni BFS, Bidirekciona pretraga
  - Dijkstra algoritam
  - Primer lavirinta i trazenja puta
  - Definisanje stanja, strukture stabla pretrazivanja, evaluacija strategija za pretragu prostora stanja i resavanje problema slepim pretragama

## 3) 13.3.2019. sreda
  - Vodjene pretrage, pohlepna pretraga, A i A* 
  - Primer: lavirint i slagalica

## 4) 20.3.2019. sreda
  - Resavanje problema primenom algoritama pretrazivanja prostora. 
  - Primer SAH
  - Uvod u evolutivno racunarstvo 

## 5) 27.3.2019. sreda
  - Genetski algoritmi
  - Problem trgovackog putnika
  - Algoritam simuliranog kaljenja

## 6) 3.4.2019. sreda
    - Kolokvijum
    - ocena 6: teorijski test, materijali sa predavanja + knjiga prof Milosavljevic
    - ocena 8: implementacija algoritma A* i BFS ili DFS na primeru lavirinta
    - ocena 10: implementacija algoritama A* i BFS na komplikovanijim primerima ili implementacija problema trgovackog putnika 

## 7) 10.4.2019. sreda
  - Masinsko ucenje - uvod
  - Ucenje sa uciteljem, regresija, klasifikacija
    KNN, stabla odlucivanja, Random forest, SVM
  - obucavajuci skupovi..
  - scikit-learn 

## 8) 17.4.2019. sreda
  - Ucenje bez ucitelja, klasterizacija, 
  - kMeans, 
  - DbSCAN

## 9) 24.4.2019. sreda
  - Neuronske mreze - uvod
  - linearna algebra/graf izracunavanja
  - pytorch/tensorflow/keras
  - Viseslojni perceptron - implementacija u pytorch
  - opencv/torchvision/

## 10) 1.5.2019. sreda
  - neradna nedelja/praznici
## 11) 8.5.2019. sreda
  - Konvolutivne neuronske mreze
  - Reinforcement learning,...
  - transformacija i priprema podataka za obradu i treniranje
  - primeri: MNIST i CIFAR-10 ili CIFAR-100

## 12) 15.5.2019. sreda
    - Kolokvijum II
    - ocena 6: teorijski test, materijali sa predavanja 
    - ocena 8: implementacija viseslojnog perceptrona u pytorch ili keras okruzenju
    - ocena 10: implementacija NN na primeru prepoznavanja objekata na slikama

## 13) 22.5.2019. sreda
    Zavrsni projekti/ priprema
## 14) 29.5.2019. sreda
    Zavrsni projekti/ priprema
## 15) 5.6.2019. sreda
    Zavrsni projekti/ priprema
