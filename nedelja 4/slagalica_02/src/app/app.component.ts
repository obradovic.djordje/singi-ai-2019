import { Component } from '@angular/core';

import {Stanje} from './stanje';
import {TNode} from './node';
import {Problem} from './problem';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

  stanje:Stanje = new Stanje();
  resenje:TNode;
  listaStanja:Stanje[];
  poruka = "";
  korak = 0;


  prikazDugme(n:number):string{
    if(n == 9){
      return "-";
    }else{
      return ""+n;
    }
  }


  prviUSirinu():void{
    this.poruka = "";
    this.resenje = null;
    this.listaStanja = [];
    this.korak = 0;
    // let stanjeA = new Stanje();
    // let stanjeB = new Stanje();
    // console.log(stanjeA.equals(stanjeB));

    let problem = new Problem();
    let pocetak = new TNode();
    pocetak.stanje = this.stanje.copy();
    pocetak.parent = null;

    let front = [pocetak];

    let it = 0;
    while(front.length>0){
      console.log(front.length);
      let tekuci = front.pop();
      if(problem.isGoal(tekuci.stanje)){
        // RESILI !!!!!
        this.resenje = tekuci;
        this.poruka = "RESENO "+it;
        console.log("RESIO !!!!!");
        break;
      }else{
        let sledecaStanja = problem.sledecaStanja(tekuci.stanje);
        for(let i in sledecaStanja){
          let st = sledecaStanja[i];
          if(tekuci.obradjeno(st) == false){
            let cvor = new TNode();
            cvor.stanje = st;
            cvor.parent = tekuci;
            front.unshift(cvor);
          }
        }
      }
      it++;
      if(it>1000000)
        break;
    }
    if(this.resenje){
      this.listaStanja = [];
      this.korak = 0;
      let tekuci = this.resenje;
      while(tekuci != null){
        this.listaStanja.unshift(tekuci.stanje);
        tekuci = tekuci.parent;
      }
    }
  }

  prviUDubinu():void{
    const MAX_DUBINA = 5;
    this.poruka = "";
    this.resenje = null;
    this.listaStanja = [];
    this.korak = 0;
    // let stanjeA = new Stanje();
    // let stanjeB = new Stanje();
    // console.log(stanjeA.equals(stanjeB));

    let problem = new Problem();
    let pocetak = new TNode();
    pocetak.stanje = this.stanje.copy();
    pocetak.parent = null;
    pocetak.dubina = 0;

    let front = [pocetak];

    for(let MAX_DUBINA=5; MAX_DUBINA<1000; MAX_DUBINA+=5){
      let it = 0;
      while(front.length>0){
        console.log(front.length);
        let tekuci = front.pop();
        if(problem.isGoal(tekuci.stanje)){
          // RESILI !!!!!
          this.resenje = tekuci;
          this.poruka = "RESENO "+it;
          console.log("RESIO !!!!!");
          break;
        }else{
          let sledecaStanja = problem.sledecaStanja(tekuci.stanje);
          for(let i in sledecaStanja){
            let st = sledecaStanja[i];
            if(tekuci.obradjeno(st) == false){
              let cvor = new TNode();
              cvor.stanje = st;
              cvor.parent = tekuci;
              cvor.dubina = tekuci.dubina+1;
              //front.unshift(cvor);
              if(cvor.dubina<=MAX_DUBINA)
                front.push(cvor);
            }
          }
        }
        it++;
        if(it>1000000)
          break;
      }
      if(this.resenje!=null)
        break;
    }
    if(this.resenje){
      this.listaStanja = [];
      this.korak = 0;
      let tekuci = this.resenje;
      while(tekuci != null){
        this.listaStanja.unshift(tekuci.stanje);
        tekuci = tekuci.parent;
      }
    }
  }



  prikaziKorak(smer:number):void{
    let stanje = this.listaStanja[this.korak];
    this.stanje = stanje;
    this.korak += smer;
  }



}
