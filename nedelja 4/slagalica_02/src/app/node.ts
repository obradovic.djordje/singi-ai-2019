import {Stanje} from './stanje'


export class TNode{

    parent:TNode = null;
    stanje:Stanje;
    dubina:number;


    obradjeno(stanje:Stanje):boolean{
        if(this.parent == null){
            return false;
        }else{
            if(this.stanje.equals(stanje))
                return true;
            else
                return this.parent.obradjeno(stanje);
        }
    }

}