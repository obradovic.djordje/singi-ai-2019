import sys
from PySide2 import QtWidgets
from core.main_window import MainWindow

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    main_window = MainWindow("Maze")
    main_window.show()
    sys.exit(app.exec_())
