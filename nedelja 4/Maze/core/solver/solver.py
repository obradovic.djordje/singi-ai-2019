import random
from .state import State


class Solver:
    def __init__(self, maze, algorithm):
        self.maze = maze
        self.algorithm = algorithm

    def get_path(self, final_state):
        path = []
        current_state = final_state
        while current_state is not None:
            path.append(current_state)
            current_state = current_state.parent_state
        path.reverse()
        return path

    def solve(self):
        start_tile = random.sample(self.maze.get_tiles_by_type("entrance"), 1)[0]
        start_state = State(start_tile["x"], start_tile["y"], start_tile, None)

        res = self.algorithm(start_state, self.maze, "exit")

        res[1].append(res[0])
        res = (self.get_path(res[0]), res[1])

        return res
